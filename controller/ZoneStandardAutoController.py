from controller.ProduitController import ProduitController
from model.Stock import Stock
from model.ZoneStandardAuto import ZoneStandardAuto
from model.utils import *

class ZoneStandardAutoController:

    def __init__(self):
        self.infoZoneStandardAuto = self.initZone()

    def initStockLocal(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0, len(list_produits)):
            varianteLogistique = ProduitController.getVarianteLogistique(ProduitController,list_produits[cptproduit].idProduit)
            if (varianteLogistique == "ZoneStandardAuto"):
                list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 20000))

        return list_stock_produits

    def initZone(self):

        return ZoneStandardAuto(7, 0.5, 1, self.initStockLocal(), None)

    def arriveeFileStandardAuto(self, infocolis):
        global QStandardAuto
        global Echeancier
        global BStandardAuto
        global DateSimu

        QStandardAuto = QStandardAuto + 1

        if QStandardAuto < 10:
            Echeancier.append([self.accessStandardAuto(infocolis), DateSimu])
            Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accessStandardAuto(self, infocolis):
        global QStandardAuto
        global BStandardAuto
        global DateSimu
        global Echeancier

        QStandardAuto = QStandardAuto - 1

        BStandardAuto = BStandardAuto + 1

        infocolis.dateSortie = infocolis.dateArrivee + self.infoZoneStandardAuto.dureeTraitement
        infocolis.statut = "prepare"
        newDate = infocolis.dateSortie


        Echeancier.append([self.departStandardAuto(infocolis), newDate])

        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def departStandardAuto(self,infoColis):
        global BStandardAuto
        global Echeancier
        global DateSimu
        global QStandardAuto

        BStandardAuto = BStandardAuto - 1

        if QStandardAuto > 0:
            Echeancier.append([self.accessStandardAuto(infoColis), DateSimu])

        Echeancier.sort(key=lambda x: x[1], reverse=False)