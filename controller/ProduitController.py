from model.Produit import Produit


class ProduitController:
    def __init__(self):
        pass
    def initProduits(self):
        list_produit = []
        list_produit.append(Produit(1,'Doliprane','ZoneStandard'))
        list_produit.append(Produit(2,'Aspirine','ZoneDetailAgv'))
        list_produit.append(Produit(3,'Alprazolam','ZoneDetailAmv'))
        list_produit.append(Produit(4,'Sertraline','ZoneDetailB'))
        list_produit.append(Produit(5,'Zolpidem','ZoneDetailAmv'))
        list_produit.append(Produit(6,'Humex','ZonePalette'))
        list_produit.append(Produit(7,'Tramadol','ZoneStandardAuto'))
        return list_produit

    def getVarianteLogistique(self,idProduit):
        list_produit = ProduitController.initProduits(self)
        cpt = 0
        for cpt in range (0,len(list_produit)):
            if(list_produit[cpt].idProduit == idProduit):
                break

        return list_produit[cpt].varianteLogistique
