from model.Commande import Commande

class CommandeController:
    def __init__(self):
        pass
    def initCommandes(self):
        list_Commande = []
        list_Commande.append(Commande(1,"Pharmacie de l'étoile",1,4,0))
        list_Commande.append(Commande(2,"Pharmacie des 2 lions",3,15,0))
        return list_Commande

    def getCommandeById(self,idCommande):
        list_Commande = self.initCommandes(self)
        for cpt in range (0,len(list_Commande)):
            if (idCommande == list_Commande[cpt].idCommande):
                return list_Commande[cpt]

        return None



