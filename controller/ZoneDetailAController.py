from controller.ProduitController import ProduitController
from model.Stock import Stock
from model.ZoneDetailAmv import ZoneDetailAmv
from model.ZoneDetailAgv import ZoneDetailAgv

from model.utils import *

'''
Classe controller de la zone detail A moyenne vente 
'''


class ZoneDetailAController:
    '''
    :param self: paramètre défaut

    '''

    def __init__(self):
        self.infoZonemv = self.initZonemv()
        self.infoZonegv = self.initZonegv()

    '''
    :param self: paramètre défaut
    :return list_stock_produits: liste de produits stockés localement dans la zone de traitement 

    '''

    def initStockLocalmv(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0, len(list_produits)):
            varianteLogistique = ProduitController.getVarianteLogistique(ProduitController,list_produits[cptproduit].idProduit)
            if (varianteLogistique == "ZoneDetailAmv"):
                list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 300))

        return list_stock_produits

    def initZonemv(self):

        return ZoneDetailAmv(1, 40, 8, self.initStockLocalmv())

    def initStockLocalgv(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0, len(list_produits)):
            varianteLogistique = ProduitController.getVarianteLogistique(ProduitController,list_produits[cptproduit].idProduit)
            if (varianteLogistique == "ZoneDetailAgv"):
                list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 300))

        return list_stock_produits

    def initZonegv(self):

        return ZoneDetailAgv(2, 20, 8, self.initStockLocalgv())

    def arriveeFileMV(self, infocolis):
        global QMV
        global Echeancier
        global BMV
        global DateSimu

        # Incrémenter le nbColis dans la file MV
        QMV = QMV + 1

        # Si MV est libre
        if BMV < 3:
            # Insertion de l'evt Acces MV à DateSimu
            Echeancier.append([self.accesMV(infocolis), DateSimu])
        # On trie
        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accesMV(self, infocolis):
        global QMV
        global BMV
        global DateSimu
        global Echeancier

        # On décrémente le nb de Colis dans la file MV
        QMV = QMV - 1

        # On occupe La zoneMV
        BMV = BMV + 1

        # MàJ de la date de sortie suivant les params de la zone
        if (infocolis.varianteLogistique == "ZoneDetailAmv"):
            infocolis.dateSortie = infocolis.dateArrivee + self.infoZonemv.dureeTraitement
            infocolis.statut = "prepare"
            newDate = infocolis.dateSortie
        else:
            newDate = DateSimu

        # print("Heure de départ", newDate)

        # Insertion dans l'échéancier
        Echeancier.append([self.arriveeFileGV(infocolis), newDate])

        # On trie
        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def arriveeFileGV(self, infocolis):
        global QGV
        global Echeancier
        global BGV
        global DateSimu
        global BMV

        # On libère MV de 1 colis
        BMV = BMV - 1
        QGV = QGV + 1

        if BGV < 3:
            Echeancier.append([self.accesGV(infocolis), DateSimu])
            # On trie
            Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accesGV(self, infocolis):
        global QGV
        global BGV
        global DateSimu
        global Echeancier

        QGV = QGV - 1

        BGV = BGV + 1

        # MàJ de la date de sortie suivant les params de la zone
        if (infocolis.varianteLogistique == "ZoneDetailAgv"):
            infocolis.dateSortie = infocolis.dateArrivee + self.infoZonegv.dureeTraitement
            infocolis.statut = "prepare"
            newDate = infocolis.dateSortie
        else:
            newDate = DateSimu

        Echeancier.append([self.departGV(infocolis), newDate])

        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def departGV(self,infoColis):
        global BGV
        global Echeancier
        global DateSimu
        global QMV

        BGV = BGV - 1

        # Si la file de la zone A est non vide
        if QMV > 0:
            # On ajoute l'evt AccessMV à DateSimu
            Echeancier.append([self.accesMV(infoColis), DateSimu])

        # On trie
        Echeancier.sort(key=lambda x: x[1], reverse=False)
