from controller.ProduitController import ProduitController
from model.Stock import Stock
from model.ZoneRamasse import ZoneRamasse
from model.utils import *

class ZoneRamasseController:

    def __init__(self):
        self.infoZoneRamasse = self.initZone()

    def initStockLocal(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0, len(list_produits)):
            varianteLogistique = ProduitController.getVarianteLogistique(ProduitController,list_produits[cptproduit].idProduit)
            if (varianteLogistique == "ZoneRamasse"):
                list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 5000))

        return list_stock_produits

    def initZone(self):

        return ZoneRamasse(6, 2, 1, self.initStockLocal())

    def arriveeFileRamasse(self, infocolis):
        global QRamasse
        global Echeancier
        global BRamasse
        global DateSimu

        QRamasse = QRamasse + 1

        if QRamasse < 2:
            Echeancier.append([self.accessRamasse(infocolis), DateSimu])
            Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accessRamasse(self, infocolis):
        global QRamasse
        global BRamasse
        global DateSimu
        global Echeancier

        QRamasse = QRamasse - 1

        BRamasse = BRamasse + 1

        infocolis.dateSortie = infocolis.dateArrivee + self.infoZoneRamasse.dureeTraitement
        infocolis.statut = "prepare"
        newDate = infocolis.dateSortie


        Echeancier.append([self.departRamasse(infocolis), newDate])

        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def departRamasse(self,infoColis):
        global BRamasse
        global Echeancier
        global DateSimu
        global QRamasse

        BRamasse = BRamasse - 1

        if QRamasse > 0:
            Echeancier.append([self.accessRamasse(infoColis), DateSimu])

        Echeancier.sort(key=lambda x: x[1], reverse=False)
