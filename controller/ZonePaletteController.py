from controller.ProduitController import ProduitController
from model.Stock import Stock
from model.ZonePalette import ZonePalette
from model.utils import *

class ZonePaletteController:

    def __init__(self):
        self.infoZonePalette = self.initZone()

    def initStockLocal(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0, len(list_produits)):
            varianteLogistique = ProduitController.getVarianteLogistique(ProduitController,list_produits[cptproduit].idProduit)
            if (varianteLogistique == "ZonePalette"):
                list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 25000))

        return list_stock_produits

    def initZone(self):

        return ZonePalette(5, 2, 1, self.initStockLocal())

    def arriveeFilePalette(self, infocolis):
        global QPalette
        global Echeancier
        global BPalette
        global DateSimu

        QPalette = QPalette + 1

        if QPalette < 4:
            Echeancier.append([self.accessPalette(infocolis), DateSimu])
            Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accessPalette(self, infocolis):
        global QPalette
        global BPalette
        global DateSimu
        global Echeancier

        QPalette = QPalette - 1

        BPalette = BPalette + 1

        infocolis.dateSortie = infocolis.dateArrivee + self.infoZonePalette.dureeTraitement
        infocolis.statut = "prepare"
        newDate = infocolis.dateSortie


        Echeancier.append([self.departPalette(infocolis), newDate])

        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def departPalette(self,infoColis):
        global BPalette
        global Echeancier
        global DateSimu
        global QPalette

        BPalette = BPalette - 1

        if QPalette > 0:
            Echeancier.append([self.accessPalette(infoColis), DateSimu])

        Echeancier.sort(key=lambda x: x[1], reverse=False)
