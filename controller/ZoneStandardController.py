from controller.ProduitController import ProduitController
from model.Stock import Stock
from model.ZoneStandard import ZoneStandard
from model.utils import *

class ZoneStandardController:

    def __init__(self):
        self.infoZoneStandard = self.initZone()

    def initStockLocal(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0, len(list_produits)):
            varianteLogistique = ProduitController.getVarianteLogistique(ProduitController,list_produits[cptproduit].idProduit)
            if (varianteLogistique == "ZoneStandard"):
                list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 300))

        return list_stock_produits

    def initZone(self):

        return ZoneStandard(3, 20, 5, self.initStockLocal())

    def arriveeFileStandard(self, infocolis):
        global QStd
        global Echeancier
        global BStd
        global DateSimu

        QStd = QStd + 1

        if BStd < 2:
            Echeancier.append([self.accesStandard(infocolis), DateSimu])
            Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accesStandard(self, infocolis):
        global QStd
        global BStd
        global DateSimu
        global Echeancier

        QStd = QStd - 1

        BStd = BStd + 1

        infocolis.dateSortie = infocolis.dateArrivee + self.infoZoneStandard.dureeTraitement
        infocolis.statut = "prepare"
        newDate = infocolis.dateSortie


        Echeancier.append([self.departStandard(infocolis), newDate])

        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def departStandard(self,infoColis):
        global BStd
        global Echeancier
        global DateSimu
        global QStd

        BStd = BStd - 1

        if QStd > 0:
            Echeancier.append([self.accesStandard(infoColis), DateSimu])

        Echeancier.sort(key=lambda x: x[1], reverse=False)
