from controller.ProduitController import ProduitController
from model.LigneCommande import LigneCommande
from model.Stock import Stock



class LigneCommandeController:
    def __init__(self):
        pass

    def initLigneCommande(self):
        list_produits = ProduitController.initProduits(ProduitController)
        list_LigneCommande = []
        '''Commande 1'''
        list_LigneCommande.append(LigneCommande(Stock(list_produits[0].idProduit,50),1))
        '''Commande 2'''
        for cpt in range (0,len(list_produits)):
            list_LigneCommande.append(LigneCommande(Stock(list_produits[cpt].idProduit, 100), 2))

        return list_LigneCommande



