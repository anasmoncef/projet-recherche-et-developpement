from controller.ProduitController import ProduitController
from model.Stock import Stock
from model.ZoneDetailB import ZoneDetailB
from model.utils import *

class ZoneDetailBController:

    def __init__(self):
        self.infoZoneDetailB = self.initZone()

    def initStockLocal(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0, len(list_produits)):
            varianteLogistique = ProduitController.getVarianteLogistique(ProduitController,list_produits[cptproduit].idProduit)
            if (varianteLogistique == "ZoneDetailB"):
                list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 300))

        return list_stock_produits

    def initZone(self):

        return ZoneDetailB(4, 10, 4, self.initStockLocal())

    def arriveeFileDetailB(self, infocolis):
        global QdetailB
        global Echeancier
        global BdetailB
        global DateSimu

        QdetailB = QdetailB + 1

        if QdetailB < 4:
            Echeancier.append([self.accesDetailB(infocolis), DateSimu])
            Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accesDetailB(self, infocolis):
        global QdetailB
        global BdetailB
        global DateSimu
        global Echeancier

        QdetailB = QdetailB - 1

        BdetailB = BdetailB + 1

        infocolis.dateSortie = infocolis.dateArrivee + self.infoZoneDetailB.dureeTraitement
        infocolis.statut = "prepare"
        newDate = infocolis.dateSortie


        Echeancier.append([self.departDetailB(infocolis), newDate])

        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def departDetailB(self,infoColis):
        global BdetailB
        global Echeancier
        global DateSimu
        global QdetailB

        BdetailB = BdetailB - 1

        if QdetailB > 0:
            Echeancier.append([self.accesDetailB(infoColis), DateSimu])

        Echeancier.sort(key=lambda x: x[1], reverse=False)
