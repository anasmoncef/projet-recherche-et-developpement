from model.Colis import Colis
from controller.LigneCommandeController import LigneCommandeController
from controller.CommandeController import CommandeController
from controller.ProduitController import ProduitController

class ColisController:
    def __init__(self):
        pass

    def initColis(self):
        list_Colis = []
        cptIdColis = 1
        list_ligneCommande = LigneCommandeController.initLigneCommande(LigneCommandeController)
        for indiceLigneCommande in range(0,len(list_ligneCommande)):
            commande = CommandeController.getCommandeById(CommandeController,list_ligneCommande[indiceLigneCommande].idCommande)
            if (commande is not None):
                for indiceProduit in range(0,len(list_ligneCommande[indiceLigneCommande].produitsCommande)):
                    variante_logistique = ProduitController.getVarianteLogistique(ProduitController, list_ligneCommande[indiceLigneCommande].produitsCommande[indiceProduit].idProduit)

                    if(len(list_Colis)==0):
                        list_Colis.append(Colis(cptIdColis,variante_logistique,commande.date_Commande,None,"nonTraite",commande.idCommande,list_ligneCommande[indiceLigneCommande].produitsCommande[indiceProduit]))
                        cptIdColis = cptIdColis + 1
                    else:
                        indiceColis = 0
                        for indiceColis in range(0,len(list_Colis)):
                            if((list_Colis[indiceColis].varianteLogistique == variante_logistique) or (list_Colis[indiceColis].varianteLogistique == "ZoneDetailAgv" and variante_logistique == "ZoneDetailAmv") or (list_Colis[indiceColis].varianteLogistique == "ZoneDetailAmv" and variante_logistique == "ZoneDetailAgv") ):
                                if (list_Colis[indiceColis].idCommande == commande.idCommande):
                                    list_Colis[indiceColis].listeProduit.append(list_ligneCommande[indiceLigneCommande].produitsCommande[indiceProduit])

                        if (indiceColis == len(list_Colis)):
                            list_Colis.append(Colis(cptIdColis, variante_logistique, commande.date_Commande, None, "nonTraite",commande.idCommande,list_ligneCommande[indiceLigneCommande].produitsCommande[indiceProduit]))

                        cptIdColis = cptIdColis + 1

        return list_Colis















