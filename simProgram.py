from model.utils import *
from controller.ColisController import ColisController
from controller.ZoneDetailAController import ZoneDetailAController
from controller.ZoneDetailBController import ZoneDetailBController
from controller.ZoneStandardController import ZoneStandardController
from controller.ZonePaletteController import ZonePaletteController
from controller.ZoneRamasseController import ZoneRamasseController
from controller.ZoneStandardAutoController import ZoneStandardAutoController

DureeSimu = 20
list_Colis = ColisController.initColis(ColisController)
cptColis = 0
nbColis = len(list_Colis)
detailA = ZoneDetailAController.__init__(ZoneDetailAController)
detailB = ZoneDetailBController.__init__(ZoneDetailBController)
standard = ZoneStandardController.__init__(ZoneStandardController)
palette = ZonePaletteController.__init__(ZonePaletteController)
ramasse = ZoneRamasseController.__init__(ZoneRamasseController)
standardauto = ZoneStandardAutoController.__init__(ZoneStandardAutoController)




def Debut():
    global DateSimu
    global Echeancier
    #print(DateSimu)

    Echeancier.append([ArriveeColis, DateSimu])
    Echeancier.append([Fin, DureeSimu])
    Echeancier.sort(key=lambda x: x[1], reverse=False)






def Simulateur():
    global DateSimu
    global Echeancier
    global QMV
    global QGV
    global BMV
    global BGV
    global QStd
    global BStd
    global QdetailB
    global BdetailB
    global QPalette
    global BPalette
    global QRamasse
    global BRamasse
    global QStandardAuto
    global BStandardAuto

    QMV = 0
    QGV = 0
    BMV = 0
    BGV = 0
    QStd = 0
    BStd = 0
    QdetailB = 0
    BdetailB = 0
    QPalette = 0
    BPalette = 0
    QRamasse = 0
    BRamasse = 0
    QStandardAuto = 0
    BStandardAuto = 0


    DateSimu = 0
    Echeancier.append([Debut, DateSimu])
    while(Echeancier):
        #On récupère le premier couple
        firstPair = Echeancier[0]

        #On récupère la date
        Date = firstPair[1]


        #On met à jour la date de simulation
        DateSimu = Date

        #On exécute l'evt Evt
        firstPair[0]()

        #On retire le couple (Evt, Date) utilisé
        if Echeancier:
            Echeancier.pop(0)


def ArriveeColis():
    global NbCommandes
    global Echeancier
    global DateSimu
    global listComm
    global cptColis
    global nbColis

    if (cptColis < nbColis):
        newDateColis = list_Colis[cptColis].dateArrivee
        Echeancier.append([ArriveeColis, newDateColis])
        # On insère le couple l'evt ArriveeFileLDA à DateSimu
        if (list_Colis[cptColis].varianteLogistique == "ZoneDetailAmv" or list_Colis[cptColis].varianteLogistique=="ZoneDetailAgv"):
            Echeancier.append([detailA.arriveeFileMV(list_Colis[cptColis]), DateSimu])
        elif (list_Colis[cptColis].varianteLogistique=="ZoneDetailB"):
            Echeancier.append([detailB.arriveeFileDetailB(list_Colis[cptColis]), DateSimu])
        elif (list_Colis[cptColis].varianteLogistique=="ZoneStandard"):
            Echeancier.append([standard.arriveeFileStandard(list_Colis[cptColis]), DateSimu])
        elif (list_Colis[cptColis].varianteLogistique=="ZonePalette"):
            Echeancier.append([palette.arriveeFilePalette(list_Colis[cptColis]), DateSimu])
        elif (list_Colis[cptColis].varianteLogistique=="ZoneRamasse"):
            Echeancier.append([ramasse.arriveeFileRamasse(list_Colis[cptColis]), DateSimu])
        elif (list_Colis[cptColis].varianteLogistique=="ZoneStandardAuto"):
            Echeancier.append([standardauto.arriveeFileStandardAuto(list_Colis[cptColis]), DateSimu])

        cptColis = cptColis + 1

        # Trier l'echancier par date afin que les evt soient stockés par ordre chronologique
        Echeancier.sort(key=lambda x: x[1], reverse=False)




def Fin():
    global Echeancier
    global AireQLDA
    global AireQLSBD
    global AireBLDA
    global QLDA
    global QLSBD
    global BLDA
    global NbCommandes
    global listtest
    global listComm

    if(Echeancier):
        Echeancier.clear()
    if(NbCommandes > 0):
        tempsMoyFileLDA = AireQLDA / NbCommandes

    tailleMoyFileLDA = AireQLDA / DureeSimu


    print("Nombre de colis total : ", nbColis)
    for y in range(0,nbColis):
        print("Date d'arrivée du colis :", y+1,"DA:",list_Colis[y].dateArrivee, "Date de finalisation des traitements : ",list_Colis[y].dateSortie)





if __name__ == "__main__":
    Simulateur()






