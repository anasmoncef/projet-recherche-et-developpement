'''
Classe représentant une commande dans le système

'''


class Commande:
    '''
    :param self: paramètre défaut
    :param idCommande: identifiant de la commande
    :param organisme: nom de la société effectuant la commande
    :param date_Commande : date de passage de la commande
    :param date_Livraison: date de livraison exigée par le client
    :param date_Livraison_Effective: date effective de sortie du centre de distribution
    '''
    def __init__(self,idCommande,organisme,date_Commande,date_Livraison,date_Livraison_Effective):
        self.idCommande = idCommande
        self.organisme = organisme
        self.date_Commande = date_Commande
        self.date_Livraison = date_Livraison
        self.date_Livraison_Effective = date_Livraison_Effective

