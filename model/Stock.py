
'''
Classe représentant un stock (local ou global)

'''


class Stock:
    '''
    :param self: paramètre défaut
    :param idProduit: identifiant du produit pharmacetique
    :param nomProduit: qte en stock du produit
    '''
    def __init__(self, idProduit, qteStock):
        self.idProduit = idProduit
        self.qteStock = qteStock
