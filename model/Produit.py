'''
Classe représentant un produit

'''


class Produit:
    '''
    :param self: paramètre défaut
    :param idProduit: identifiant du produit pharmacetique
    :param nomProduit: nom commercial du produit
    :param varianteLogistique: type de VL du produit
    '''
    def __init__(self, idProduit, nomProduit, varianteLogistique):
        self.idProduit = idProduit
        self.nomProduit = nomProduit
        self.varianteLogistique = varianteLogistique
